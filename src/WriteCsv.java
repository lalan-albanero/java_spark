import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

public class WriteCsv {
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkSession sparkSession = SparkSession.builder().appName("writeInCsv").master("local").getOrCreate();
        List<Row> rowList = new ArrayList<Row>();
        List<user> rowList1 = new ArrayList<user>();

        rowList.add(RowFactory.create("rosan", "1", "kumar"));
        rowList.add(RowFactory.create("rohan", "2", "kumar"));
        rowList1.add(new user("1", "gggg", "fff"));
        rowList1.add(new user("2", "ssss", "dddd"));

        Dataset<Row> dataset = sparkSession.createDataset(rowList, getEncoder());
        Dataset<Row> dataset1 = sparkSession.createDataFrame(rowList1, user.class);
        Dataset<Row> un = dataset.union(dataset1);
        Dataset<Row> un1 = dataset.join(dataset1, "id");
        // dataset.show();
        dataset1.show();
        // un.show();
        // un1.show();
        dataset1 = dataset1.withColumn("password", functions.abs(functions.rand().$times(1000)));
        dataset1.show();

        // drop column

        dataset = dataset.drop("password");
        dataset.show();
        // dataset.coalesce(1).write().mode(SaveMode.Overwrite).option("header",
        // true).csv("OutputPath");

        System.out.println("Successfully...");

    }

    private static ExpressionEncoder getEncoder() {
        StructType structType = new StructType();
        structType = structType.add("fname", DataTypes.StringType, true);
        structType = structType.add("id", DataTypes.StringType, false);
        structType = structType.add("lname", DataTypes.StringType, true);
        return RowEncoder.apply(structType);
    }
}
