import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructType;

import static org.apache.spark.sql.functions.col;

import org.apache.spark.sql.types.StructField;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ReadCsv {
    public static void main(String[] args) throws Exception {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        System.out.println("Hello, World!");
        // SparkSession sparkSession =
        // SparkSession.builder().appName("readCsv").master("local").getOrCreate();

        // Dataset<Row> dataRead = sparkSession.read().option("header",
        // true).csv("in/SampleCSVFile_11kb.csv");
        // dataRead.show();

        // Dataset<Row> dataRead1=sparkSession.read().option("delimiter", "
        // ").option("header",true).csv("C:\\New.csv");
        // dataRead1.show();
        // // printing schema
        // dataRead.printSchema();
        // //print one column
        // dataRead.select("b").show();
        // //incrementing column by 1
        // dataRead.select(col("c"), col("d").plus(1)).show();
        // filtering
        // dataRead.filter(col("f").gt(100)).show();
        // performing groupby and count
        // dataRead.groupBy("i").count().show();
        // filtering
        // Dataset<Row> filteredData = dataRead.filter(new Column("A").gt(15));
        // filteredData.show();

        // dataRead.createOrReplaceTempView("people");

        // Dataset<Row> sqlDF = sparkSession.sql("SELECT * FROM people");
        // sparkSession.sql("SELECT * FROM global_temp.people").show();
        // sparkSession.newSession().sql("SELECT * FROM global_temp.people").show();
        // Dataset<Row> sqlDF = sparkSession.sql("SELECT * FROM people ORDER BY c ASC");

        // sqlDF.show(15);
        SparkSession spark = SparkSession.builder().master("local").appName("ssss").getOrCreate();

        Dataset<Row> dataRead = spark.read().option("header", true).csv(
                "C:\\Users\\Lalan kumar\\Desktop\\Spark\\sparkapp\\src\\main\\resources\\static\\SampleCSVFile_11kb.csv");
        StructType a = dataRead.schema();
        System.out.println(a);
        StructField[] b = a.fields();
        for (int i = 0; i < b.length; i++) {
            System.out.println(b[i].name()+" : "+b[i].dataType());
            // System.out.println(b[i].dataType());
        }

        // dataRead.show();
    }
}
