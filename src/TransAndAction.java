import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import jodd.util.StringUtil;

public class TransAndAction {
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf sparkConf = new SparkConf().setMaster("local[*]").setAppName("TransAndAction");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        // int[] arr = { 1, 2, 3, 4, 5 };
        // List<Integer> list = new ArrayList<>();
        // list.add(1);
        // list.add(2);
        // list.add(3);
        // list.add(4);

        // JavaRDD<Integer> rdd = sc.parallelize(list);
        // rdd = rdd.map(u -> u + 5);
        // rdd.foreach(a -> System.out.println(a));
        // System.out.println();

        JavaRDD<String> input=sc.textFile("in/SampleCSVFile_11kb.csv");
        JavaRDD<List> input1 = input.map(line->Arrays.asList(line.split(",")).subList(1, 3));
        JavaRDD<String>input2 = input1.map(line->StringUtil.join(line, " => "));
        input2.foreach(line->System.out.println(line));

        sc.close();

    }
}