
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

import java.util.*;

public class WordCount {
	private static  void wordCount(String fileName) {
		System.out.println(fileName);
		
		SparkConf sparkConf= new SparkConf().setMaster("local").setAppName("WordCounter");
		
		JavaSparkContext sparkContext=new JavaSparkContext(sparkConf);
		
		JavaRDD<String> inputFile=sparkContext.textFile(fileName);
		
		JavaRDD<String> wordFromFile=inputFile.flatMap(cont->Arrays.asList(cont.split(" ")).iterator());
		
		
		JavaPairRDD countData= wordFromFile
				.mapToPair(t-> new Tuple2(t,1))
				.reduceByKey((x,y)->(int)x+(int)y);
		
		countData.saveAsTextFile("OutputPath");
		
		sparkContext.close();
	}
	public static void main(String[] args) {
		if(args.length==0) {
			System.out.println("please give a source file....");
		}else {
		
		wordCount(args[0]);
		}
	}
	
}
